$(document).ready(function(){


	/* Function Template
	-----------------------*/


	/**/


	/* Global variables
	-----------------------*/

	var screenHeight, screenWidth, $window;

	$window = $(window);



	/* Debug
	-----------------------*/

	$(document).keydown(function(e){
		if( e.keyCode == 32){

			// If 'd' is pressed do:

		}
	});


	/* Layout
	-----------------------*/

	var layout = function(){
		screenHeight = $window.innerHeight();
		screenWidth = $window.innerWidth();
	}


	/* WebStorage
	-----------------------*/

		var hasWebStorage = function(){
			try {
				mod = new Date;
				localStorage.setItem(mod, mod.toString());
				console.log(localStorage.getItem(mod));
				result = localStorage.getItem(mod) == mod.toString();
				localStorage.removeItem(mod);
				console.log(result);
				return result;
			} catch (e) {
				console.log("dit gaat er fout: " + e);
			} finally {
				console.log("yep");
			}
		}
		hasWebStorage();

	/* Slideshow
	-----------------------*/

	function slideshow(){
		if( $(".slideshow").length > 0 ){
			var slides = $(".slideshow-container .data-slideshow li").length;
			var slideshow = 0;
			var slideData = [];
			var currentSlide = 0;
			var previousSlide = 0;
			var debounced = 1;
			var loop;
			var settings = {
				timer: 6000,
				transition: 1500,
			}

		if(slides > 1){

			for(var i=0; i < slides; i++){
				slideData[i] = $(".slideshow-container .data-slideshow li").eq(i).html();
				$(".slideshow-container .indicators").append("<li></li>");
			}

			$(".slideshow-container .indicators li").eq(0).addClass('active');

			$(".slideshow-container").swipe({
				swipeLeft: function(event, direction, distance, duration, fingerCount) {
					if(debounced){
						debounced = 0;
						looper(false);
						currentSlide++;
						if(currentSlide > slides-1){
							currentSlide=0;
						}
						changeSlide(currentSlide, "next");
						looper(true);
					}
				},
				swipeRight: function(event, direction, distance, duration, fingerCount) {
					if(debounced){
						debounced = 0;
						looper(false);
						currentSlide--;
						if(currentSlide < 0){
							currentSlide = slides-1;
						}
						changeSlide(currentSlide, "prev");
						looper(true);
					}
				},
				threshold: 20,
			});

			$(".slideshow-container .next").click(function(){
				if(debounced){
					debounced = 0;
					looper(false);
					currentSlide++;
					if(currentSlide > slides-1){
						currentSlide=0;
					}
					changeSlide(currentSlide, "next");
					looper(true);
				}
			});

			$(".slideshow-container .prev").click(function(){
				if(debounced){
					debounced = 0;
					looper(false);
					currentSlide--;
					if(currentSlide < 0){
						currentSlide = slides-1;
					}
					changeSlide(currentSlide, "prev");
					looper(true);
				}
			});

			$(".slideshow-container .indicators li").click(function(){
				if(debounced){
					debounced = 0;
					looper(false);
					currentSlide = $(this).index();
					if(currentSlide > slides){
						currentSlide = 0;
					} else if(currentSlide < 0){
						currentSlide = slides-1;
					}
					if(currentSlide > previousSlide){
						changeSlide(currentSlide, "next");
					} else if( currentSlide < previousSlide){
						changeSlide(currentSlide, "prev");
					} else{
						debounced = 1;
					}
					looper(true);
				}
			});


			function looper(bool){
				if(bool){
					loop = setInterval(function(){
						currentSlide++;
						if( currentSlide > slides-1 ){
							currentSlide = 0;
							changeSlide(currentSlide, "next");
						} else {
							changeSlide(currentSlide, "next");
						}
					}, settings.timer);
				} else {
					clearInterval(loop);
				}
			} looper(true);

			function changeSlide(position, direction){

				if(direction == "next"){
					$(".slideshow").prepend('<li>'+slideData[position]+'</li>');

					$(".slideshow li").eq(0).addClass('prepNext');
					$(".slideshow li").eq(1).addClass('removePrev');
					setTimeout(function(){
						$(".slideshow li").eq(0).removeClass('prepNext');
					}, 100);

					setTimeout(function(){
						$(".slideshow li").eq(1).removeClass('removePrev');
					}, settings.transition + 100);


				} else if(direction == "prev"){
					$(".slideshow").prepend('<li>'+slideData[position]+'</li>');

					$(".slideshow li").eq(0).addClass('prepPrev');
					$(".slideshow li").eq(1).addClass('removeNext');
					setTimeout(function(){
						$(".slideshow li").eq(0).removeClass('prepPrev');
					}, 100)
					setTimeout(function(){
						$(".slideshow li").eq(0).removeClass('removeNext');
					},settings.transition + 100);


				}

				setTimeout(function(){
					$(".slideshow-container .indicators li").removeClass("active");
					$(".slideshow-container .indicators li").eq(position).addClass("active");
				}, settings.transition - 500);

				setTimeout(function(){
					debounced = 1;
					$(".slideshow li").eq(1).remove();
				},settings.transition + 100);
				previousSlide = currentSlide;
			}
		}

		}
	}




	/* Form
	-----------------------*/

	$(document).foundation({
		abide: {
			timeout: 400
		}
	});

	$('#form')
	  .on('invalid.fndtn.abide', function () {

	  })
	  .on('valid.fndtn.abide', function () {
	    $.post('/contactengine.php?ajax', $('#form').serialize(), function(data) {
		    $('#formSuccess').slideUp(0);
		    $('#formSuccess').html(data);
		    $('#formSuccess').slideDown(500);
		    $("#form").animate({opacity:0},350);
		    $("#form").slideUp(1000,"swing", layout());
		  }, 'text');
	  });


	  $('input, textarea').placeholder();




	/* Google Maps
	-----------------------*/

	if($(".contact #map-canvas").length > 0){
		//var lat = $(".data.coordinaten_contact").attr('data-lat');
		//var lon = $(".data.coordinaten_contact").attr('data-lon');

		 function initialize() {

	        var myLatlng = new google.maps.LatLng(lat,lon);

	        var mapOptions = {
	          center: myLatlng,
	          zoom: 10,
	          styles: [{"featureType":"all","elementType":"all","stylers":[{"saturation":-100},{"gamma":0.5}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#f5f5f5"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#919191"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.fill","stylers":[{"color":"#f8f8f8"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#f7f7f7"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"#f2f2f2"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry.fill","stylers":[{"color":"#e1e1e1"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"saturation":"0"}]},{"featureType": "road","elementType": "labels","stylers": [{ "visibility": "off" }]},{"featureType":"water","elementType":"all","stylers":[{"color":"#c9c9c9"}]}],
	          scrollwheel: false,
	          zoomControl: true,
	          panControl: false,
	          streetViewControl: false,
	          draggable: false,
	          zoomControlOptions:{
	          	position: google.maps.ControlPosition.LEFT_BOTTOM
	          },
	          panControlOptions:{
	          	position: google.maps.ControlPosition.RIGHT_CENTER
	          },
	        };

	        var map = new google.maps.Map(document.getElementById("map-canvas"),
	            mapOptions);

	        var marker = new google.maps.Marker({
	            position: myLatlng,
	            animation: google.maps.Animation.DROP,
	            map: map,
	            icon: '[MARKER PATH]',
	            title:"[WEBSITE TITLE]"
	        });
	        marker.setMap(map);

	   }
	   google.maps.event.addDomListener(window, 'load', initialize);
	}




	/* Plugins
	-----------------------*/

		/* Foundation */

		$(document).foundation({
		  equalizer : {
		    equalize_on_stack: true
		  }
		});


		/* Fancybox */


		if($(".fancybox").length > 0){

			$(".fancybox").fancybox({
				helpers : {
			        title: {
			            type: 'inside',
			            position: 'top'
			        },
				    overlay: {
				      locked: false
				    }
			    },
			    margin 		: [20, 60, 20, 60],
			    width 		: 960,
			    height 		: 540,
			    aspectRatio : true,
			    padding 	: 0

			});
		}




	/* Initialize functions
	-----------------------*/

	layout();


	$window.resize(layout());



});

'use strict';

angular.module('todoListApp')

.directive('todos', function(){
	return{
		templateUrl : '/assets/templates/todos.html',
		controller : 'mainCtrl',
		//replace : true
	}
});